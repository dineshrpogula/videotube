//package com.dinesh.videotube;
//
//import android.content.Context;
//import android.content.Entity;
//import android.graphics.Bitmap;
//import android.util.Log;
//import android.util.LruCache;
//
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.HurlStack;
//import com.android.volley.toolbox.ImageLoader;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.google.gson.Gson;
//
//import java.io.IOException;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
///**
// * Created by dinesh on 1/14/2017.
// */
//
//public class VideoSearch implements Response.Listener<String>, Response.ErrorListener {
//
//
//    private static volatile VideoSearch videoSearch;
//    private Context context;
//    private ImageLoader imageLoader;
//    private RequestQueue mrequestQueue;
//    private VideoSearch(Context context) {
//        this.context = context;
//        mrequestQueue = getRequestQueue();
//        imageLoader = new ImageLoader(mrequestQueue,
//                new ImageLoader.ImageCache() {
//                    private final LruCache<String, Bitmap>
//                            cache = new LruCache<String, Bitmap>(20);
//
//                    @Override
//                    public Bitmap getBitmap(String url) {
//                        return cache.get(url);
//                    }
//
//                    @Override
//                    public void putBitmap(String url, Bitmap bitmap) {
//
//                        cache.put(url, bitmap);
//                    }
//                });
//    }
//
//    public RequestQueue getRequestQueue() {
//        if (mrequestQueue == null) {
//            mrequestQueue = Volley.newRequestQueue(context, new HurlStack() {
//                @Override
//                protected HttpURLConnection createConnection(URL url) throws IOException {
//                    HttpURLConnection connection = super.createConnection(url);
//                    connection.setInstanceFollowRedirects(false);
//                    return connection;
//                }
//            });
//            mrequestQueue.start();
//        }
//        return mrequestQueue;
//    }
//    public static VideoSearch getInstance(Context context) {
//        if (videoSearch == null)
//            videoSearch = new VideoSearch(context);
//        return videoSearch;
//    }
//
//    /**
//     * This method is used to search the tracks based in search text and entity.
//     *
//     * @param search         search item.
//     * @param entity         entity
//     * @param resultListener ResultListener for results.
//     */
//    public void search(String search, Entity entity, ResultListener resultListener) {
//        this.resultListener = resultListener;
//        search = search.trim().toLowerCase();
//        search = search.replaceAll(" ", "+");
//        String url = "https://gdata.youtube.com/feeds/api/videos?q=vevo&max-re%E2%80%8C%E2%80%8Bsults=5&v=2&alt=jsonc&orderby=published";
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, this, this);
//        mrequestQueue.add(stringRequest);
//    }
//
//    /**
//     * This method will parse the JSON string form url and give Tracks.
//     *
//     * @param json
//     * @return instace of Tracks.
//     */
//    public Tracks getTracks(String json) {
//        Gson gson = new Gson();
//        Tracks tracks = gson.fromJson(json, Tracks.class);
//        return tracks;
//    }
//
//    /**
//     * This method will give the ImageLoader.
//     *
//     * @return
//     */
//    public ImageLoader getImageLoader() {
//        return imageLoader;
//    }
//
//    /**
//     * This Interface is used to subscribe for the results.
//     */
//    public interface ResultListener {
//        void onResults(VideoActivity[] tracks);
//
//        void onError(VolleyError error);
//    }
//
//    private ResultListener resultListener;
//
//    @Override
//    public void onResponse(String response) {
//        Log.d("Response is ", response);
//        VideoActivity[] results = getTracks(response).results;
//        Log.d("Response is ", results.length + "");
//        if (resultListener != null) resultListener.onResults(results);
//    }
//
//    @Override
//    public void onErrorResponse(VolleyError error) {
//        error.printStackTrace();
//        if (resultListener != null) resultListener.onError(error);
//    }
//
//
//
//
//}
