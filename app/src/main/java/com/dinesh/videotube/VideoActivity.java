package com.dinesh.videotube;

import android.app.Activity;
import android.app.ProgressDialog;

import android.content.Context;
import android.media.Image;
import android.media.audiofx.AudioEffect;
import android.os.Bundle;
import android.os.PersistableBundle;

import android.support.v7.app.AppCompatActivity;
import android.util.EventLogTags;
import android.widget.ListView;
import android.widget.MediaController;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dinesh on 1/14/2017.
 */
public class VideoActivity extends Activity {
    private static final String url="https://gdata.youtube.com/feeds/api/videos?q=vevo&max-re%E2%80%8C %E2%80%8Bsults=5&v=2&alt=jsonc&orderby=published";
    private final Context context;
    private ProgressDialog dialog;
    private List<Tracks> array = new ArrayList<>();
    private ItemAdapter adapter;
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.track_list);
        ListView listview = (ListView) findViewById(R.id.TrackList);
        adapter= new ItemAdapter(this, array);
        listview.setAdapter(adapter);
        dialog= new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, url,  null, new Response.Listener<JSONArray>(){
            @Override
            public void onResponse(JSONArray response) {
            hideDialog();
            int i=0;

                while(i<response.length()){
                    try{

                        JSONObject obj= response.getJSONObject(i);
                        Tracks track = new Tracks();
                        track.setTitle(obj.getString("title"));
                        track.setImage(obj.getString("image"));
                        track.setDescription(obj.getString("description"));
                        StringBuilder builder = new StringBuilder();
                        builder.append(array.get(i)+",");
                        array.add(track);
                        i++;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VideoVolley.getmInstance().addToRequestQueue(jsonArrayRequest);

        }
    public VideoActivity(Context context){
        this.context = context;
    }
    public void hideDialog(){
        if(dialog != null){
            dialog.dismiss();
            dialog = null;
        }
    }

}
