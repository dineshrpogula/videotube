package com.dinesh.videotube;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

/**
 * Created by dinesh on 1/21/2017.
 */

public class ItemAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Tracks> tracks;

    ImageLoader imageLoader =VideoVolley.getmInstance().getimageLoader();
    public ItemAdapter(Activity activity,List<Tracks> tracks){
        this.activity=activity;
        this.tracks=tracks;
    }
    @Override
    public int getCount() {
        return tracks.size();
    }

    @Override
    public Object getItem(int position) {
        return tracks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater==null){
            inflater=(LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }
        if(convertView==null){
            convertView=inflater.inflate(R.layout.track_list ,null);
        }
        if(imageLoader==null){
            imageLoader= VideoVolley.getmInstance().getimageLoader();
            NetworkImageView imageView =(NetworkImageView) convertView.findViewById(R.id.videoThumb);
            TextView title=(TextView) convertView.findViewById(R.id.titleName);
            TextView decription=(TextView) convertView.findViewById(R.id.titleDescription);
            Tracks  track= tracks.get(position);
            imageView.setImageUrl(track.getImage(), imageLoader);
            title.setText(track.getTitle());
            decription.setText(track.getDescription());
        }
        return convertView;
    }
}
