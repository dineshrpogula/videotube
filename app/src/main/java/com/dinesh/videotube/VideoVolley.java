package com.dinesh.videotube;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import static android.content.ContentValues.TAG;

/**
 * Created by dinesh on 1/20/2017.
 */

public class VideoVolley extends Application{
    private ImageLoader imageLoader;
    private RequestQueue mrequestQueue;
    private static VideoVolley mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance= this;
    }

    public static synchronized VideoVolley getmInstance() {
        if(mInstance==null){
            mInstance= new VideoVolley();
        }

        return mInstance;

    }
     private RequestQueue getmrequestQueue(){
         if(mrequestQueue== null){
             mrequestQueue= Volley.newRequestQueue(getApplicationContext());
         }
         return mrequestQueue;
     }
    public ImageLoader getimageLoader(){
        getmrequestQueue();
        if(imageLoader== null){
            imageLoader= new ImageLoader(this.mrequestQueue,new BitmapCache());
        }
        return this.imageLoader;
    }
//    public <T> void addToRequestQueue(Request<T> request, String tag){
//        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
//        getmrequestQueue().add(request);
//    }

    public <T> void addToRequestQueue(Request<T> request){
        request.setTag(TAG);
        getmrequestQueue().add(request);
    }
    public void cancelPendingReQuest(Object tag){
        if(mrequestQueue!= null){
            mrequestQueue.cancelAll(tag);
        }
    }

}
