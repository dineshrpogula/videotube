package com.dinesh.videotube;

import java.util.ArrayList;

/**
 * Created by dinesh on 1/14/2017.
 */
public class Tracks {
    public String title;
    public String image;
    public String description;

//    public  Tracks(String title,String image, String description){
//        this.image= image;
//        this.title= title;
//        this.description=description;
//    }

    public String getTitle(){
        return title;
    }
    public String getImage(){
        return image;
    }
    public  String getDescription(){
        return  description;
    }
    public  void setTitle(String title){
        this.title=title;
    }
    public void setImage (String image){
        this.image=image;
    }
    public void setDescription(String description){
        this.description= description;
    }

}
